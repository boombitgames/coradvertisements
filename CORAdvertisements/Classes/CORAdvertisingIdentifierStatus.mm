#import <AdSupport/AdSupport.h>

extern "C" {

    /*
     Return Advertising Identifier, it will consist of all zeros if it is not available.
     */
    char* corGetAdvertisingIdentifier()
    {
        const char* data = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString].UTF8String;
        char* ret = (char*) malloc(strlen(data) + 1);
        strcpy(ret, data);
        return ret;
    }

}
