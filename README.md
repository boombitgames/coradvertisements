# CORAdvertisements

[![CI Status](https://img.shields.io/travis/Tomasz Pilarski/CORAdvertisements.svg?style=flat)](https://travis-ci.org/Tomasz Pilarski/CORAdvertisements)
[![Version](https://img.shields.io/cocoapods/v/CORAdvertisements.svg?style=flat)](https://cocoapods.org/pods/CORAdvertisements)
[![License](https://img.shields.io/cocoapods/l/CORAdvertisements.svg?style=flat)](https://cocoapods.org/pods/CORAdvertisements)
[![Platform](https://img.shields.io/cocoapods/p/CORAdvertisements.svg?style=flat)](https://cocoapods.org/pods/CORAdvertisements)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CORAdvertisements is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CORAdvertisements'
```

## Author

Tomasz Pilarski, core@boombit.com

## License

CORAdvertisements is available under the MIT license. See the LICENSE file for more info.
